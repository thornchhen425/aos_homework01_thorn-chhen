package com.bunchhen.hw01;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class ProfileDetailsActivity extends AppCompatActivity {

    // one button
    Button btnBack;

    TextView getFullName, getEmail, getPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_details);

        btnBack = findViewById(R.id.btnGoBack);
        getFullName = findViewById(R.id.getFullName);
        getEmail = findViewById(R.id.getEmail);
        getPassword = findViewById(R.id.getPassword);

        Intent i = getIntent();

        // Store data from input CreateUserActivity
        String name = i.getStringExtra("name");
        String email = i.getStringExtra("email");
        String password = i.getStringExtra("password");

        // Set display date on ProfileDetailsActivity
        getFullName.setText(name);
        getEmail.setText(email);
        getPassword.setText(password);

        // Button back to CreateUserActivity
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), CreateUserActivity.class);
                startActivity(i);

                // pass alert massage to CreateUserActivity
                Toast.makeText(ProfileDetailsActivity.this, "Please create a new profile again", Toast.LENGTH_LONG).show();
            }
        });

    }
}